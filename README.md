# recipe-app-api-proxy

NGINX reverse proxy for recipe-app API.

## Usage

### Environment variables

- `LISTEN_PORT` - Port to listen on (Defaults to `8000`)
- `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
- `APP_PORT` - Port of the app to forward requests to (default: `9000`)


## `gitlab.ci.yml` breakdown

```yaml
image: docker:19.03.05
services:
  - docker:19.03.05-dind

stages:
  - Build
  - Push

# this Build is called a Job
Build:
  # You can group jobs into stages
  stage: Build
  before_script: []
  # actual script we run for this job
  script:
    # create new dir called `data`
    - mkdir data/
    # `docker build --compress` is suggested for preformat production builds
    # The `-t` flag is used to name the image
    - docker build --compress -t proxy .
    # save the image to the `data` dir and this is done so that we can pass the image as an artifact to the next job. Artifacts are a way to pass files from one job to another job.
    # By default every job will have a fresh file system.
    - docker save --output data/image.tar proxy
  artifacts:
    name: image
    paths:
      - data/
```
